const express = require("express")
const app=express()
const db = require("./db/db.config2")
const crud=require("./app/crud")
const bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())




  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:4200");
     res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
    next();
  });
  
    
  app.post("/mylist/:userid", crud.createList)
  app.post("/mylist/taches", crud.createTache)
  app.get("/mylist/:id",crud.findListById)
  app.get("/mylist/all",crud.findAllList)

    app.listen(4545,()=>{
      console.log(`server is running at 4545`);
  });
   

  module.exports=app;