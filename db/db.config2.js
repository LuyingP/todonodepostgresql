const dbConfig = require("../config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: false
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.lists = require("./modellist.js")(sequelize, Sequelize);
db.taches = require("./modeltache.js")(sequelize, Sequelize);

db.lists.hasMany(db.taches, { as: "taches" });
db.taches.belongsTo(db.lists, {
  foreignKey: "listofId",
  as: "list",
});

module.exports = db;