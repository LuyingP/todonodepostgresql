module.exports = (sequelize, DataTypes) => {
    const Tache = sequelize.define("tache", {
     description_courte:{
        type:DataTypes.STRING,
        allowNull: false},
    
    description: {
        type: DataTypes.STRING},
    
    date_fin: {
        type: DataTypes.DATE,
        allowNull: false},

    date_creation: {
        type: DataTypes.DATE}
    });
  
    return Tache;
  };





