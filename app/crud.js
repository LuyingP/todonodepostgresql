const db = require("../db/db.config2");
const List = db.lists;
const Tache = db.taches;
const express = require("express")
const app=express()
const bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

const createList = (req,res) => {
    return List.create({
      titre: req.body.titre,
      userid: req.params.userid,
    })
      .then((list) => {
        console.log(">> Created list: " + JSON.stringify(list,null, 4));
        res.send("ok");
        return list;
      })
      .catch((err) => {
          res.send(err.message)
        console.log(">> Error while creating list: ", err);
      });
  };

  const createTache = (req,res) => {
    return Tache.create({
      description_courte: req.body.description_courte,
      description: req.body.description,
      listofId: req.parmas.listofId,
      date_fin: new Date(req.body.date_fin),
      date_creation: new Date(req.body.date_creation)
    })
      .then((tache) => {
        console.log(">> Created tache: " + JSON.stringify(tache, null, 4));
        res.send("ok");
        return tache;
      })
      .catch((err) => {
        res.send(err.message)
        console.log(">> Error while creating tache: ", err);
      });
  };

  const findListById = (req,res) => {
      const id=req.params.id
      console.log("*********"+ id)
    return List.findByPk(id, { include: ["taches"] })
      .then((list) => {
          res.status(200).send(list)
        return list;
      })
      .catch((err) => {
        res.status(404).send(err.message)
        console.log(">> Error while finding list: ", err);
      });
  };

  const findAllList = (req,res) => {
    return List.findAll({
      include: ["taches"],
    }).then((lists) => {
        res.send(lists);
      return lists;
    });
  }

exports.createList = createList;
exports.createTache=createTache;
exports.findListById=findListById;
exports.findAllList=findAllList;