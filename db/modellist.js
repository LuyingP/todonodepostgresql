module.exports = (sequelize, DataTypes) => {
    const List = sequelize.define("list", {

        userid:{
            type:DataTypes.STRING,
            allowNull: false},
                
        titre: {
            type: DataTypes.STRING,
            allowNull: false}
    });
  
    return List;
  };


